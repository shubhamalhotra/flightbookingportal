<!-- story 4- when all the seats are free -->
select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats
 from flight_route as fr
  join aeroplane as ap on fr.aeroplane_id = ap.id
      join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
          where fr.source = 'Hyderabad'
           and fr.destination ='Mumbai'
           and fr.travel_date = '2019-09-12'
              and atc.travelclass = sa.travel_class
                 and sa.travel_class ='ECONOMY_CLASS';


<!--Story 5 - when 100 of total seats are available  -->
 select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats from flight_route as fr
        join aeroplane as ap on fr.aeroplane_id = ap.id
        join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
        where fr.source = 'Hyderabad'
            and fr.destination ='Mumbai'
            and fr.travel_date = '2019-09-10'
            and atc.travelclass = sa.travel_class
            and sa.travel_class ='ECONOMY_CLASS';


<!--story 6 - when Monday Friday or sunday rates will be 40% extra 11 sep is friday and available only one month before.-->
select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats from flight_route as fr
        join aeroplane as ap on fr.aeroplane_id = ap.id
        join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
        where fr.source = 'Hyderabad'
            and fr.destination ='Mumbai'
            and fr.travel_date = '2019-09-12'
            and atc.travelclass = sa.travel_class
            and sa.travel_class ='BUSINESS_CLASS';

<!-- story 7 - booking available 10 days before depart date -->
 select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats from flight_route as fr
        join aeroplane as ap on fr.aeroplane_id = ap.id
        join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
        where fr.source = 'Hyderabad'
            and fr.destination ='Mumbai'
            and fr.travel_date = '2019-09-12'
            and atc.travelclass = sa.travel_class
            and sa.travel_class ='FIRST_CLASS';




