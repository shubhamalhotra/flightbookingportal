$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();
    });
});

function fire_ajax_submit() {

    var search = {};
    search["origin"] = $("#toValue").val();
    search["destination"] = $("#fromValue").val();
    search["numOfPassengers"] = $("#numOfPassengers").val();
    search["departDate"] = $("#departDate").val();
    search["travelClass"] = $("#travelClass").val();
    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/flights/searchflight",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            if(!($('#flightTable tbody').empty()))
            {
                $('#flightTable tbody').remove();
            }
            getPlaneInfo(data);
        }
    });
}

function getPlaneInfo(data) {
    var tr;
    var $flightDiv = $('#flightdiv');
    var $messageDiv = $('#noFlightMsg'); // get the reference of the div
    var availableFlightDetailResult = data.flightDetailResult;

    if(data.msg === "Success") {
        $flightDiv.show();
        $messageDiv.hide();
        for (var i = 0; i < availableFlightDetailResult.length; i++) {

            var availableFlightDetail = availableFlightDetailResult[i].flightDetail;
            tr = $('<tr/>');
            tr.append("<td>" + availableFlightDetail.flightName + "</td>");
            //tr.append("<td>" + availableFlightDetail.modelName + "</td>");
            tr.append("<td>" + availableFlightDetail.origin + "</td>");
            tr.append("<td>" + availableFlightDetail.destination + "</td>");
            tr.append("<td>" + availableFlightDetail.departDate + "</td>");
            tr.append("<td>" + availableFlightDetailResult[i].finalCalculatedPrice + "</td>");
            $('#flightTable tbody').append(tr);
        }
    }
    else {
        $flightDiv.hide();
        $messageDiv.show();
        $messageDiv.show().html(data.msg.fontcolor('Red').fontsize(5)); // show and set the message
    }
}





