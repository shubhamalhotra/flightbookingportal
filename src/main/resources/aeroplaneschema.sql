<!--areoplane-->
create table if not exists aeroplane(id int(10) not null auto_increment,
 modelname varchar(30) not null,
Primary key(id, modelname)
);

Insert into aeroplane (id,modelname) values (default,'AIRBUS_A319_V2');
Insert into aeroplane (id,modelname) values (default,'AIRBUS_A321');
Insert into aeroplane (id,modelname) values (default,'BOEING_777_200LR_77L');



<!--schedule flightDetail-->
create table if not exists aeroplane_travel_class(id int(10) not null auto_increment,
 travelclass varchar(30) not null,
 totalseats int(3) not null,
 aeroplane_id int(10) not null,
 primary key (id, aeroplane_id),
 foreign key fk_aeroplane_id(aeroplane_id)
 references aeroplane(id)
);

Insert into aeroplane_travel_class (id,travelclass,totalseats,aeroplane_id) values (default,'ECONOMY_CLASS',144,1);
Insert into aeroplane_travel_class (id,travelclass,totalseats,aeroplane_id) values (default,'ECONOMY_CLASS',152,2);
Insert into aeroplane_travel_class (id,travelclass,totalseats,aeroplane_id) values (default,'BUSINESS_CLASS',20,2);
Insert into aeroplane_travel_class (id,travelclass,totalseats,aeroplane_id) values (default,'ECONOMY_CLASS',195,3);
Insert into aeroplane_travel_class (id,travelclass,totalseats,aeroplane_id) values (default,'BUSINESS_CLASS',35,3);
Insert into aeroplane_travel_class (id,travelclass,totalseats,aeroplane_id) values (default,'FIRST_CLASS',8,3);


<!-- route table-->
create table if not exists flight_route(id int(10) not null auto_increment,
    flight_name varchar(10) not null,
    source varchar(30) not null,
    destination varchar(30) not null,
    travel_date date not null,
    aeroplane_id int(10) not null,
    primary key (id, aeroplane_id,flight_name),
    foreign key fk_aeroplane_id(aeroplane_id)
    references aeroplane(id)
);

Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F001','Hyderabad','Mumbai','2019-09-10',1);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F002','Hyderabad','Mumbai','2019-09-11',1);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F003','Hyderabad','Mumbai','2019-09-12',1);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F004','Hyderabad','Mumbai','2019-09-10',2);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F005','Hyderabad','Mumbai','2019-09-13',2);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F006','Hyderabad','Mumbai','2019-09-12',2);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F007','Hyderabad','Mumbai','2019-09-10',3);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F008','Hyderabad','Mumbai','2019-09-11',3);
Insert into flight_route (id,flight_name,source,destination,travel_date,aeroplane_id) values (default,'F009','Hyderabad','Mumbai','2019-09-12',3);

<!-- booking status -->
create table if not exists seat_availability(seat_available_id int(10) not null auto_increment,
    available_seats int(3) not null,
    travel_class varchar(30) not null,
    flight_name varchar(10) not null,
    primary key (seat_available_id)

);

Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,120,'ECONOMY_CLASS','F001');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,100,'ECONOMY_CLASS','F002');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,140,'ECONOMY_CLASS','F003');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,120,'ECONOMY_CLASS','F004');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,15,'BUSINESS_CLASS','F005');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,20,'BUSINESS_CLASS','F006');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,90,'ECONOMY_CLASS','F007');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,20,'BUSINESS_CLASS','F008');
Insert into seat_availability (seat_available_id,available_seats,travel_class,flight_name) values (default,5,'FIRST_CLASS','F009');


<!--query -->
 select * from flight_route as fr
 join aeroplane as ap on fr.aeroplane_id = ap.id
 join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
 join seat_availability as sa on sa.aeroplane_id = ap.id
 where fr.source = "Hyderabad"
 and fr.destination ="Mumbai"
 and fr.travel_date = "2019-09-10"
 and atc.travelclass = sa.travel_class
 and sa.travel_class ='ECONOMY_CLASS';


select * from flight_route as fr
 join aeroplane as ap on fr.aeroplane_id = ap.id
 join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
 join seat_availability as sa on sa.flight_name = fr.flight_name
 where fr.source = "Hyderabad"
 and fr.destination ="Mumbai"
 and fr.travel_date = "2019-09-10"
 and atc.travelclass = sa.travel_class
 and sa.travel_class ='ECONOMY_CLASS';


 select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats from flight_route as fr
        join aeroplane as ap on fr.aeroplane_id = ap.id
        join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
        where fr.source = 'Hyderabad'
            and fr.destination ='Mumbai'
            and fr.travel_date = '2019-09-10'
            and atc.travelclass = sa.travel_class
            and sa.travel_class ='ECONOMY_CLASS';


select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats from flight_route as fr
        join aeroplane as ap on fr.aeroplane_id = ap.id
        join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
        where fr.source = 'Hyderabad'
            and fr.destination ='Mumbai'
            and fr.travel_date = '2019-09-11'
            and atc.travelclass = sa.travel_class
            and sa.travel_class ='BUSINESS_CLASS';


select fr.flight_name,fr.source, fr.destination, fr.travel_date, sa.available_seats, atc.travelclass, ap.modelname, atc.totalseats from flight_route as fr
        join aeroplane as ap on fr.aeroplane_id = ap.id
        join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id
        join seat_availability as sa on sa.flight_name = fr.flight_name
        where fr.source = 'Hyderabad'
            and fr.destination ='Mumbai'
            and fr.travel_date = '2019-08-30'
            and atc.travelclass = sa.travel_class
            and sa.travel_class ='FIRST_CLASS';


