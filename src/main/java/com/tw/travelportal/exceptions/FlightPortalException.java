package com.tw.travelportal.exceptions;

public class FlightPortalException extends RuntimeException {


    public FlightPortalException(String message, Throwable cause) {
        super(message, cause);
    }
    public FlightPortalException(String message) {
        super(message);
    }

}
