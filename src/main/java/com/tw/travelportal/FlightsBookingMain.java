package com.tw.travelportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightsBookingMain {

    public static void main(String[] args)
    {
        SpringApplication.run(FlightsBookingMain.class, args);
    }

}
