package com.tw.travelportal.services;

import com.tw.travelportal.constant.Constant;
import com.tw.travelportal.utility.BasePriceLoader;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.temporal.ChronoUnit.DAYS;

public class FirstClassPriceCalculator extends PriceCalculator {

    private String travelDate;
    public FirstClassPriceCalculator(int numOfPassengers, int totalSeats, int availableSeats, String modelName, String travelClass, String travelDate, BasePriceLoader basePriceLoader) {
        super(numOfPassengers, totalSeats, availableSeats, modelName, travelClass, basePriceLoader);
        this.travelDate = travelDate;
    }

    private double calculateBasePriceForFirstClass() {

        long days = daysBetweenDates();
        if(days > Constant.TEN) {
            return Constant.ZERO;
        }
        else if(days == Constant.TEN)
        {
            return getBasePrice();
        }
        else {
            long gapDays =  Constant.TEN - days;
            return (getBasePrice() + (getBasePrice() * (gapDays*Constant.TEN))/Constant.HUNDRED);
        }
    }

    private long daysBetweenDates()
    {
        LocalDate startDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //convert String to LocalDate
        LocalDate localTravelDate = LocalDate.parse(travelDate, formatter);
        long daysBetween = DAYS.between(startDate, localTravelDate);
        return daysBetween;
    }

    public double calculateTotalPrice() {
        double totalPrice = Constant.ZERO;
        if (checkDateInRangeFirst()) {
                    totalPrice = calculateBasePriceForFirstClass() * numOfPassengers;
                } else {
                    return Constant.ZERO;
                }

        return totalPrice;
    }

    private boolean checkDateInRangeFirst()
    {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = LocalDate.now().plusDays(Constant.TEN);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //convert String to LocalDate
        LocalDate localTravelDate = LocalDate.parse(travelDate, formatter);
        return localTravelDate.isAfter(startDate) && localTravelDate.isBefore(endDate);
    }

}
