package com.tw.travelportal.services;

import com.tw.travelportal.constant.Constant;
import com.tw.travelportal.enums.TRAVEL_CLASS;
import com.tw.travelportal.exceptions.FlightPortalException;
import com.tw.travelportal.model.FlightDetail;
import com.tw.travelportal.model.FlightDetailWithCalculatedPrice;
import com.tw.travelportal.model.SearchCriteria;
import com.tw.travelportal.repository.FlightRepository;
import com.tw.travelportal.utility.BasePriceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class FlightSearchService {
    @Autowired
    private FlightRepository repository;

    @Autowired
    private BasePriceLoader basePriceLoader;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private List<FlightDetailWithCalculatedPrice> getAvailableFlights(SearchCriteria searchCriteria) {

        List<FlightDetailWithCalculatedPrice> finalListOfFlightsWithPrice = new ArrayList<>();

        List<FlightDetail> flightList = repository.getAvailableFlights(searchCriteria);
        for (Iterator<FlightDetail> flightDetailIterator = flightList.iterator(); flightDetailIterator.hasNext(); ) {

            FlightDetail flightDetail = flightDetailIterator.next();
            if (flightDetail.getAvailableSeats() < searchCriteria.getNumOfPassengers()) {
                flightDetailIterator.remove();
            } else {

                populateFlightsWithPrice(searchCriteria, finalListOfFlightsWithPrice, flightDetail);
            }
        }
        return finalListOfFlightsWithPrice;

    }

    private void populateFlightsWithPrice(SearchCriteria searchCriteria, List<FlightDetailWithCalculatedPrice> finalListOfFlightsWithPrice,
                                                                FlightDetail flightDetail) {
        PriceCalculator priceCalculator = initCalculatorClass(searchCriteria.getNumOfPassengers(),
                flightDetail.getTotalSeats(), flightDetail.getAvailableSeats(),
                flightDetail.getModelName(), searchCriteria.getTravelClass(),
                searchCriteria.getDepartDate(), basePriceLoader);

        double finalCalculatedPrice = priceCalculator.calculateTotalPrice();

        FlightDetailWithCalculatedPrice availableFlightWithPrice =
                new FlightDetailWithCalculatedPrice(flightDetail, finalCalculatedPrice);

        finalListOfFlightsWithPrice.add(availableFlightWithPrice);
    }

    public List<FlightDetailWithCalculatedPrice> getFlights(SearchCriteria searchCriteria) {
        
    	validateSourceAndDestination(searchCriteria);
    	
    	validateDepartDate(searchCriteria);
    	
    	applyDefaultValues(searchCriteria);

        List<FlightDetailWithCalculatedPrice> flightDetails = getAvailableFlights(searchCriteria);

        return flightDetails;
    }
    
    private void validateDepartDate(SearchCriteria searchCriteria)
	{
    	LocalDate localDate = LocalDate.now();
    	DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd");	
    	
    	LocalDate userDate = LocalDate.parse(searchCriteria.getDepartDate(), formatter);
    	
		if(userDate.isBefore(localDate))
		{
			throw new FlightPortalException("Date cannot be in past.");
		}
	}
    
    private void validateSourceAndDestination(SearchCriteria searchCriteria)
    {
    	if("".equals(searchCriteria.getOrigin()))
    	{
    		throw new FlightPortalException("Source cannot be empty.");
    	}
    	else if("".equals(searchCriteria.getDestination()))
    	{
    		throw new FlightPortalException("Destination cannot be empty.");
    	}
    	else if(searchCriteria.getOrigin().equalsIgnoreCase(searchCriteria.getDestination())) {
    		
    		throw new FlightPortalException("Source and Destination cannot be same.");
		}
    	
    	
    }

    private void applyDefaultValues(SearchCriteria searchCriteria)
    {
        int numOfPassengers = searchCriteria.getNumOfPassengers();
        if (numOfPassengers == Constant.ZERO) {
            searchCriteria.setNumOfPassengers(Constant.ONE);
        }
        if (searchCriteria.getDepartDate() == "") {

            LocalDate localDate = LocalDate.now().plusDays(Constant.ONE);
            searchCriteria.setDepartDate(localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }

    }

    private PriceCalculator initCalculatorClass(int numOfPassengers, int totalSeats, int availableSeats, String modelName, String travelClass,
                                               String travelDate, BasePriceLoader basePriceLoader) {
        if (travelClass.equals(TRAVEL_CLASS.ECONOMY_CLASS.name())) {
            return new EconomyPriceCalculator(numOfPassengers, totalSeats, availableSeats, modelName, travelClass,
                    travelDate, basePriceLoader);
        } else if (travelClass.equals(TRAVEL_CLASS.BUSINESS_CLASS.name())) {
            return new BusinessPriceCalculator(numOfPassengers, totalSeats, availableSeats, modelName, travelClass,
                    travelDate, basePriceLoader);
        } else if (travelClass.equals(TRAVEL_CLASS.FIRST_CLASS.name())) {
            return new FirstClassPriceCalculator(numOfPassengers, totalSeats, availableSeats, modelName, travelClass,
                    travelDate, basePriceLoader);
        }
        else {
            throw new FlightPortalException("unknown class found");
        }

    }


}
