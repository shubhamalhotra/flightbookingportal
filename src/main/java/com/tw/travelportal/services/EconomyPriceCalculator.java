package com.tw.travelportal.services;

import com.tw.travelportal.constant.Constant;
import com.tw.travelportal.utility.BasePriceLoader;

public class EconomyPriceCalculator extends PriceCalculator{

    public EconomyPriceCalculator(int numOfPassengers, int totalSeats, int availableSeats, String modelName, String travelClass, String travelDate, BasePriceLoader basePriceLoader) {
        super(numOfPassengers, totalSeats, availableSeats, modelName, travelClass, basePriceLoader);
    }

    private double calculateBasePriceForEconomy() {
        double availability = ((availableSeats * Constant.HUNDRED) / totalSeats);
        if (availability > Constant.SIXTY) {
            return getBasePrice();
        } else if (availability > Constant.TEN && availability <= Constant.SIXTY) {
            return (getBasePrice() + (Constant.THIRTY * getBasePrice()) / Constant.HUNDRED);
        } else if (availability <= Constant.TEN) {
            return (getBasePrice() + (Constant.SIXTY * getBasePrice()) / Constant.HUNDRED);
        }
        return Constant.ZERO;
    }

    public double calculateTotalPrice() {

        double totalPrice = Constant.ZERO;
        totalPrice = calculateBasePriceForEconomy() * numOfPassengers;
        return totalPrice;
    }

}
