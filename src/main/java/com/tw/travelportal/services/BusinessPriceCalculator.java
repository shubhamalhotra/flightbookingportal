package com.tw.travelportal.services;

import com.tw.travelportal.constant.Constant;
import com.tw.travelportal.utility.BasePriceLoader;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

public class BusinessPriceCalculator extends PriceCalculator {

    private String travelDate;
    public BusinessPriceCalculator(int numOfPassengers, int totalSeats, int availableSeats, String modelName, String travelClass, String travelDate, BasePriceLoader basePriceLoader) {
        super(numOfPassengers, totalSeats, availableSeats, modelName, travelClass, basePriceLoader);
        this.travelDate = travelDate;
    }

    private  double calculateBasePriceForBusiness()
    {
        return  (getBasePrice() + (Constant.FOURTY * getBasePrice()) / Constant.HUNDRED);
    }

    public double calculateTotalPrice() {
        double totalPrice = Constant.ZERO;
        if (checkDateInRangeBusiness()) {
                String day = getDayFromDate();
                if (day.equalsIgnoreCase(Constant.MON) || day.equalsIgnoreCase(Constant.FRI) ||
                        day.equalsIgnoreCase(Constant.SUN)) {
                    totalPrice = calculateBasePriceForBusiness() * numOfPassengers;
                } else {
                    totalPrice = getBasePrice() * numOfPassengers;
                }
            } else {
                return Constant.ZERO;
            }
    return totalPrice;
    }

    private boolean checkDateInRangeBusiness()
    {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = LocalDate.now().plusDays(Constant.THIRTY);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //convert String to LocalDate
        LocalDate localTravelDate = LocalDate.parse(travelDate, formatter);
        return localTravelDate.isAfter(startDate) && localTravelDate.isBefore(endDate);
    }


    private String getDayFromDate() {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(travelDate,dateTimeFormatter);
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        String day = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.US);
        return day;
    }

}
