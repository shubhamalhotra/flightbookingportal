package com.tw.travelportal.services;

import com.tw.travelportal.utility.BasePriceLoader;


public abstract class PriceCalculator {

    protected int numOfPassengers;
    protected int totalSeats;
    protected int availableSeats;
    protected String modelName;
    protected String travelClass;
    //protected double totalPrice;

    protected BasePriceLoader basePriceLoader;

    public PriceCalculator(int numOfPassengers,int totalSeats,int availableSeats,String modelName,String travelClass
                                ,BasePriceLoader basePriceLoader)
    {
        this.numOfPassengers = numOfPassengers;
        this.totalSeats = totalSeats;
        this.availableSeats = availableSeats;
        this.modelName = modelName;
        this.travelClass = travelClass;
        this.basePriceLoader = basePriceLoader;

    }

    protected double getBasePrice() {

        double basePrice = basePriceLoader.getBasePrice(travelClass, modelName);
        return basePrice;
    }

    public abstract double calculateTotalPrice() ;
}





