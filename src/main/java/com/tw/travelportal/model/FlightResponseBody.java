package com.tw.travelportal.model;

import java.util.List;

public class FlightResponseBody {

    private String msg;
    private  List<FlightDetailWithCalculatedPrice> flightDetailResult;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<FlightDetailWithCalculatedPrice> getFlightDetailResult() {
        return flightDetailResult;
    }

    public void setFlightDetailResult(List<FlightDetailWithCalculatedPrice> flightDetailResult) {

        this.flightDetailResult = flightDetailResult;
    }
}
