package com.tw.travelportal.model;

import javax.validation.constraints.NotBlank;

public class SearchCriteria {


    @NotBlank
            (message = "Source can't be empty!")
    private String origin;
    @NotBlank
            (message = "Destination can't be empty!")
    private String destination;

    private String travelClass;

    private int numOfPassengers;
    private String departDate;

    //getter setter
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNumOfPassengers() {
        return numOfPassengers;
    }

    public void setNumOfPassengers(int numOfPassengers) {
        this.numOfPassengers = numOfPassengers;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getDepartDate(){return  departDate;}

    public String getTravelClass() { return travelClass; }

    public void setTravelClass(String travelClass) { this.travelClass = travelClass; }



}
