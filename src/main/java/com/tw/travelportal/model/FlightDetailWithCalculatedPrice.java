package com.tw.travelportal.model;

public class FlightDetailWithCalculatedPrice {


    private FlightDetail flightDetail;
    private double finalCalculatedPrice;

    public FlightDetailWithCalculatedPrice(FlightDetail flightDetail, double finalCalculatedPricePrice)
    {
        this.flightDetail = flightDetail;
        this.finalCalculatedPrice = finalCalculatedPricePrice;
    }

    public FlightDetail getFlightDetail() {
        return flightDetail;
    }

    public void setFlightDetail(FlightDetail flightDetail) {
        this.flightDetail = flightDetail;
    }

    public double getFinalCalculatedPrice() {
        return finalCalculatedPrice;
    }

    public void setFinalCalculatedPrice(double finalCalculatedPrice) {
        this.finalCalculatedPrice = finalCalculatedPrice;
    }
}
