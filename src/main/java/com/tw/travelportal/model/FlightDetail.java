package com.tw.travelportal.model;

public class FlightDetail {

    private String flightName;
    private String origin;
    private String destination;
    private String departDate;
    private String modelName;
    private int totalSeats;
    private int availableSeats;

    public FlightDetail(String flightName, String origin, String destination, String departDate, String modelName, int totalSeats, int availableSeats) {
        this.flightName = flightName;
        this.origin = origin;
        this.destination = destination;
        this.departDate = departDate;
        this.modelName = modelName;
        this.totalSeats = totalSeats;
        this.availableSeats = availableSeats;
    }

    //flight name
    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    //departure date
    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String  departDate) {
        this.departDate = departDate;
    }

  //Origin
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    //Destination
    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

}
