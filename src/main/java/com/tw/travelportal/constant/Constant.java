package com.tw.travelportal.constant;

public class Constant {

    public static final int FOURTY = 40;
    public static final int SIXTY = 60;
    public static final int THIRTY = 30;
    public static final int TEN = 10;
    public static final int HUNDRED = 100;
    public static final int ZERO =0;
    public static final int ONE =1;
    public static final String MON = "Monday";
    public static final String FRI = "Friday";
    public static final String SUN = "Sunday";

}
