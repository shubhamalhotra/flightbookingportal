package com.tw.travelportal.repository;

import com.tw.travelportal.exceptions.FlightPortalException;
import com.tw.travelportal.model.FlightDetail;
import com.tw.travelportal.model.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class FlightRepository implements ObjectRepository<List<FlightDetail>>
{
    @Autowired
    public JdbcTemplate jdbcTemplate;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public List<FlightDetail> getAvailableFlights(SearchCriteria searchCriteria)  {

        List<FlightDetail> flightDetailList = new ArrayList<>();

        try {
            String sql = "select fr.flight_name as flightName, fr.source, fr.destination, fr.travel_date as travelDate ," +
                    "sa.available_seats as availableSeats, ap.modelname as modelName, atc.totalseats as totalSeats from flight_route as fr " +
            "join aeroplane as ap on fr.aeroplane_id = ap.id " +
            "join aeroplane_travel_class as atc on atc.aeroplane_id = ap.id "+
            "join seat_availability as sa on sa.flight_name = fr.flight_name "+
            "where fr.source = "+ "'" + searchCriteria.getOrigin() +"'" +
            "and fr.destination ="+ "'" + searchCriteria.getDestination() + "'" +
            "and fr.travel_date = "+ "'" + searchCriteria.getDepartDate() + "'" +
            "and atc.travelclass = sa.travel_class "+
            "and sa.travel_class =" + "'" + searchCriteria.getTravelClass() + "'";


            List<Map<String,Object>> flightList = jdbcTemplate.queryForList(sql);

            for(Map<String, Object> entry:flightList)
            {
                String flightName = (String)entry.get("flightName");
                String source = (String) entry.get("source");
                String destination = (String)entry.get("destination");
                Date travelDate = (Date)entry.get("travelDate");
                String travelDateToString = sdf.format(travelDate);
                String modelName = (String)entry.get("modelName");
                Integer totalSeats = (Integer) entry.get("totalSeats");
                Integer availableSeats  = (Integer)entry.get("availableSeats");


                FlightDetail flightDetail = new FlightDetail(flightName,source,destination,travelDateToString,modelName,totalSeats,availableSeats);
                flightDetailList.add(flightDetail);

            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            throw new FlightPortalException("error while executing query. " , ex);
        }
        return flightDetailList;
    }


}
