package com.tw.travelportal.repository;

import com.tw.travelportal.model.SearchCriteria;

public interface ObjectRepository<T> {

    public T getAvailableFlights(SearchCriteria searchCriteria);


}


