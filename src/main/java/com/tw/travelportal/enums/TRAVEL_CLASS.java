package com.tw.travelportal.enums;

public enum TRAVEL_CLASS {

    ECONOMY_CLASS,
    BUSINESS_CLASS,
    FIRST_CLASS;
}
