package com.tw.travelportal.enums;

public enum AEROPLANE_MODEL {

    BOEING_777_200LR_77L,
    AIRBUS_A321,
    AIRBUS_A319_V2;

}
