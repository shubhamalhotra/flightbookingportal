package com.tw.travelportal.utility;

import com.tw.travelportal.enums.AEROPLANE_MODEL;
import com.tw.travelportal.enums.TRAVEL_CLASS;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class BasePriceLoader {

    private static Map<AEROPLANE_MODEL, Map<TRAVEL_CLASS,Double>> basePriceMapper = null;

    public double getBasePrice(String travelClass, String modelName)
    {
        if(basePriceMapper == null)
        {
            basePriceMapper = new HashMap<>();
            loadBasePrice();
        }

        Map<TRAVEL_CLASS,Double> classMapper = basePriceMapper.get(AEROPLANE_MODEL.valueOf(modelName));
        double basePrice = classMapper.get(TRAVEL_CLASS.valueOf(travelClass));
        return basePrice;
    }

    private static Map<AEROPLANE_MODEL, Map<TRAVEL_CLASS,Double>> loadBasePrice()
    {
        basePriceMapper.put(AEROPLANE_MODEL.AIRBUS_A319_V2, loadAirbus319BasePrice());
        basePriceMapper.put(AEROPLANE_MODEL.AIRBUS_A321, loadAirbus321BasePrice());
        basePriceMapper.put(AEROPLANE_MODEL.BOEING_777_200LR_77L, loadBoeingBasePrice());
        return basePriceMapper;
    }

    private static Map<TRAVEL_CLASS,Double> loadBoeingBasePrice()
    {
        Map<TRAVEL_CLASS, Double> travelClassPriceMapper = new HashMap<>();

        travelClassPriceMapper.put(TRAVEL_CLASS.ECONOMY_CLASS, Double.valueOf(6000));
        travelClassPriceMapper.put(TRAVEL_CLASS.BUSINESS_CLASS, Double.valueOf(13000));
        travelClassPriceMapper.put(TRAVEL_CLASS.FIRST_CLASS, Double.valueOf(20000));
        return travelClassPriceMapper;
    }

    private static Map<TRAVEL_CLASS,Double> loadAirbus321BasePrice()
    {
        Map<TRAVEL_CLASS, Double> travelClassPriceMapper = new HashMap<>();

        travelClassPriceMapper.put(TRAVEL_CLASS.ECONOMY_CLASS, Double.valueOf(5000));
        travelClassPriceMapper.put(TRAVEL_CLASS.BUSINESS_CLASS, Double.valueOf(10000));
        return travelClassPriceMapper;
    }

    private static Map<TRAVEL_CLASS,Double> loadAirbus319BasePrice()
    {
        Map<TRAVEL_CLASS, Double> travelClassPriceMapper = new HashMap<>();
        travelClassPriceMapper.put(TRAVEL_CLASS.ECONOMY_CLASS, Double.valueOf(4000));
        return travelClassPriceMapper;
    }
}
