package com.tw.travelportal.controllers;

import com.tw.travelportal.model.FlightDetailWithCalculatedPrice;
import com.tw.travelportal.model.FlightResponseBody;
import com.tw.travelportal.model.SearchCriteria;
import com.tw.travelportal.services.FlightSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class SearchFlightController {

    @Autowired
    private FlightSearchService flightSearchService;

    @PostMapping("/flights/searchflight")
    public ResponseEntity<?> getSearchResultViaAjax(@Valid @RequestBody SearchCriteria searchCriteria, Errors errors) {


            FlightResponseBody result = new FlightResponseBody();

            //If error, just return a 400 bad request, along with the error message
            if (errors.hasErrors()) {

                result.setMsg(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(",")));
                return ResponseEntity.badRequest().body(result);
            }

            List<FlightDetailWithCalculatedPrice> flightDetails = flightSearchService.getFlights(searchCriteria);


            if (flightDetails != null && flightDetails.size() == 0) {
                result.setMsg("No flights available for select criteria");
                result.setFlightDetailResult(flightDetails);
            } else {
                result.setMsg("Success");
                result.setFlightDetailResult(flightDetails);
            }
            return ResponseEntity.ok(result);

    }
}
