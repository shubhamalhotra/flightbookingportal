package com.tw.travelportal.test;

import com.tw.travelportal.enums.AEROPLANE_MODEL;
import com.tw.travelportal.enums.TRAVEL_CLASS;
import com.tw.travelportal.utility.BasePriceLoader;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BasePriceLoaderUnitTest {

    private BasePriceLoader basePriceLoader = new BasePriceLoader();
    private String modelName;
    private String travelClass;

    @Test
    public void findBasePriceForBoeingAndEcomonyClass()
    {
        modelName = AEROPLANE_MODEL.BOEING_777_200LR_77L.name();
        travelClass = TRAVEL_CLASS.ECONOMY_CLASS.name();

        assert (6000.0 == basePriceLoader.getBasePrice(travelClass, modelName));

    }

    @Test
    public void findBasePriceForBoeingAndBusinessClass()
    {
        modelName = AEROPLANE_MODEL.BOEING_777_200LR_77L.name();
        travelClass = TRAVEL_CLASS.BUSINESS_CLASS.name();

        assert (13000.0 == basePriceLoader.getBasePrice(travelClass, modelName));

    }

    @Test
    public void findBasePriceForBoeingAndFirstClass()
    {
        modelName = AEROPLANE_MODEL.BOEING_777_200LR_77L.name();
        travelClass = TRAVEL_CLASS.FIRST_CLASS.name();

        assert (20000.0 == basePriceLoader.getBasePrice(travelClass, modelName));

    }

    @Test
    public void findBasePriceForAirbu321AndEconomyClass()
    {
        modelName = AEROPLANE_MODEL.AIRBUS_A321.name();
        travelClass = TRAVEL_CLASS.ECONOMY_CLASS.name();

        assert (5000.0 == basePriceLoader.getBasePrice(travelClass, modelName));

    }
    @Test
    public void findBasePriceForAirbu321AndBusinessClass()
    {
        modelName = AEROPLANE_MODEL.AIRBUS_A321.name();
        travelClass = TRAVEL_CLASS.BUSINESS_CLASS.name();

        assert (10000.0 == basePriceLoader.getBasePrice(travelClass, modelName));

    }

    @Test
    public void findBasePriceForAirbus319AndEconomyClass()
    {
        modelName = AEROPLANE_MODEL.AIRBUS_A319_V2.name();
        travelClass = TRAVEL_CLASS.ECONOMY_CLASS.name();

        assert (4000.0 == basePriceLoader.getBasePrice(travelClass, modelName));

    }
}
