package com.tw.travelportal.test;

import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


public class DateIncrementUnitTest {

    @Test
    public void incrementToNextDate(){

        Date todaysDate = new Date();
        LocalDate localDate = LocalDate.now().plusDays(1);
        Date incrementDate = java.util.Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        assert incrementDate.compareTo(todaysDate) > 0;

    }

}
