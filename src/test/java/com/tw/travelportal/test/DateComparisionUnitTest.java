package com.tw.travelportal.test;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateComparisionUnitTest {

    // equal dates test pass
    @Test
    public void compare2SimilarDatesTest() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date1 = sdf.parse("31/12/2019 00:00:00");
        Date date2 = sdf.parse("31/12/2019 00:00:00");
        int result = date1.compareTo(date2);

        //if dates are equal result is 0.
        assert result == 0;
    }

    @Test
    public void compareWhenDate1GreaterThanDate2Test() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date1 = sdf.parse("31/12/2019 00:00:00");
        Date date2 = sdf.parse("1/12/2019 00:00:00");
        int result = date1.compareTo(date2);

        //if date1 is greater than date2  result is greater than 0
        assert result > 0;
    }


    @Test
    public void compareWhenDate2GreaterThanDate1Test() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date1 = sdf.parse("1/12/2019 00:00:00");
        Date date2 = sdf.parse("31/12/2019 00:00:00");
        int result = date1.compareTo(date2);

        //if date2 is greater than date1  result is less than 0
        assert result < 0;
    }
}
