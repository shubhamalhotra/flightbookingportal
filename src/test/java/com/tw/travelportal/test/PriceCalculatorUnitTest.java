package com.tw.travelportal.test;

import com.tw.travelportal.enums.AEROPLANE_MODEL;
import com.tw.travelportal.enums.TRAVEL_CLASS;
import com.tw.travelportal.services.BusinessPriceCalculator;
import com.tw.travelportal.services.EconomyPriceCalculator;
import com.tw.travelportal.services.FirstClassPriceCalculator;
import com.tw.travelportal.services.PriceCalculator;
import com.tw.travelportal.utility.BasePriceLoader;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PriceCalculatorUnitTest {


    @Test
    public void priceCalculatorForEconomyTest()
    {
        int numOfPassenger =2;
        int totalSeats = 195;
        int availablSeats = 95;
        String modelName = AEROPLANE_MODEL.BOEING_777_200LR_77L.name();
        String travelClass = TRAVEL_CLASS.ECONOMY_CLASS.name();
        String travelDate = "2019-09-10";

        BasePriceLoader basePriceLoader = new BasePriceLoader();
        PriceCalculator priceCalculator =  new EconomyPriceCalculator(numOfPassenger,totalSeats,availablSeats, modelName, travelClass,travelDate,basePriceLoader);
        double totalPrice = priceCalculator.calculateTotalPrice();
        assert(totalPrice == 15600.0);

    }

    @Test
    public void priceCalculatorForBusinessTest()
    {
        int numOfPassenger =1;
        int totalSeats = 35;
        int availablSeats = 30;
        String modelName = AEROPLANE_MODEL.BOEING_777_200LR_77L.name();
        String travelClass = TRAVEL_CLASS.BUSINESS_CLASS.name();
        String travelDate = "2019-09-13";

        BasePriceLoader basePriceLoader = new BasePriceLoader();
        PriceCalculator priceCalculator =  new BusinessPriceCalculator(numOfPassenger,totalSeats,availablSeats, modelName, travelClass,travelDate,basePriceLoader);
        double totalPrice = priceCalculator.calculateTotalPrice();
        assert(totalPrice == 18200.0);
    }

    @Ignore
    @Test
    public void priceCalculatorForFirstTest()
    {
        int numOfPassenger =1;
        int totalSeats = 8;
        int availablSeats = 5;
        String modelName = AEROPLANE_MODEL.BOEING_777_200LR_77L.name();
        String travelClass = TRAVEL_CLASS.FIRST_CLASS.name();
        String travelDate = "2019-08-31";

        BasePriceLoader basePriceLoader = new BasePriceLoader();
        PriceCalculator priceCalculator =  new FirstClassPriceCalculator(numOfPassenger,totalSeats,availablSeats, modelName, travelClass,travelDate,basePriceLoader);
        double totalPrice = priceCalculator.calculateTotalPrice();
        assert(totalPrice == 36000.0);
    }
}
