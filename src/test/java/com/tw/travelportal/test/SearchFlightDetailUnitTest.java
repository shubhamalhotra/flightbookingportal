package com.tw.travelportal.test;

import com.tw.travelportal.enums.AEROPLANE_MODEL;
import com.tw.travelportal.enums.TRAVEL_CLASS;
import com.tw.travelportal.exceptions.FlightPortalException;
import com.tw.travelportal.model.FlightDetail;
import com.tw.travelportal.model.FlightDetailWithCalculatedPrice;
import com.tw.travelportal.model.SearchCriteria;
import com.tw.travelportal.repository.FlightRepository;
import com.tw.travelportal.services.FlightSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SearchFlightDetailUnitTest {

    @Autowired
    private FlightSearchService flightSearchService;

    @Mock
    private FlightRepository flightRepository;

    // This method is used to test whether it returns flights
    // based on Source , Destination, No of passengers and Departure date
    @Test
    public void shouldReturnFlightListWhenMatchesAllCriteriaGiven(){

        List<FlightDetail> expectedFlightList = new ArrayList<>();

        FlightDetail expectedFlightDetailList1 = new FlightDetail("F007","Hyderabad",
                "Mumbai","2019-09-10",
                AEROPLANE_MODEL.BOEING_777_200LR_77L.name(),195,100);

        FlightDetail expectedFlightDetailList2 = new FlightDetail("F004","Hyderabad",
                "Mumbai","2019-09-10",
                AEROPLANE_MODEL.AIRBUS_A321.name(),152,120);

        FlightDetail expectedFlightDetailList3 = new FlightDetail("F001","Hyderabad",
                "Mumbai","2019-09-10",
                AEROPLANE_MODEL.AIRBUS_A319_V2.name(),144,120);


        expectedFlightList.add(expectedFlightDetailList1);
        expectedFlightList.add(expectedFlightDetailList2);
        expectedFlightList.add(expectedFlightDetailList3);

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Hyderabad");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(2);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());

        when(flightRepository.getAvailableFlights(searchCriteria)).thenReturn(expectedFlightList);
         List<FlightDetailWithCalculatedPrice> filteredFlights = flightSearchService.getFlights(searchCriteria);

        assertEquals(3,filteredFlights.size());
    }

    // This method is used to test whether it returns no flights
    // when wrong Source , Destination and No of passengers given
    @Test
    public void shouldReturnNoFlightsWhenSourceAndDestinationDoesNotMatchWithAnyFlight()
    {

        List<FlightDetail> expectedFlightList = new ArrayList<>();

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Delhi");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(2);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());

        when(flightRepository.getAvailableFlights(searchCriteria)).thenReturn(expectedFlightList);
        List<FlightDetailWithCalculatedPrice> filteredFlights = flightSearchService.getFlights(searchCriteria);

        assertEquals(0,filteredFlights.size());
    }

    @Test
    public void whenNumberOfPassengersExceedsTotalSeatsTest()
    {

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Hyderabad");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(200);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());

        List<FlightDetailWithCalculatedPrice> availableList = flightSearchService.getFlights(searchCriteria);

        assertEquals(0, availableList.size());
    }
    
    @Test
    public void testWhenSourceIsEmpty()
    {
    	
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("");
        searchCriteria.setDestination("Delhi");
        searchCriteria.setNumOfPassengers(1);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());
        
        try
        {
        List<FlightDetailWithCalculatedPrice> filteredFlights = flightSearchService.getFlights(searchCriteria);
        }
        catch (FlightPortalException ex) {
		
        	assertEquals("Source cannot be empty.", ex.getMessage());
		}
  
    }
    
    @Test
    public void testWhenDestinationIsEmpty()
    {
    	
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Delhi");
        searchCriteria.setDestination("");
        searchCriteria.setNumOfPassengers(1);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());
        
        try
        {
        List<FlightDetailWithCalculatedPrice> filteredFlights = flightSearchService.getFlights(searchCriteria);
        }
        catch (FlightPortalException ex) {
		
        	assertEquals("Destination cannot be empty.", ex.getMessage());
		}
  
    }

    @Test
    public void testWhenSourceAndDestinationAreSame()
    {
    	
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Delhi");
        searchCriteria.setDestination("Delhi");
        searchCriteria.setNumOfPassengers(1);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());
        
        try
        {
        List<FlightDetailWithCalculatedPrice> filteredFlights = flightSearchService.getFlights(searchCriteria);
        }
        catch (FlightPortalException ex) {
		
        	assertEquals("Source and Destination cannot be same.", ex.getMessage());
		}
  
    }
    
    @Test
    public void testWhenDepartDateIsPastDate()
    {
    	
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Delhi");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(1);
        searchCriteria.setDepartDate("2019-07-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());
        
        try
        {
        List<FlightDetailWithCalculatedPrice> filteredFlights = flightSearchService.getFlights(searchCriteria);
        }
        catch (FlightPortalException ex) {
		
        	assertEquals("Date cannot be in past.", ex.getMessage());
		}
  
    }

}



