package com.tw.travelportal.test;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class JunitLifeCycleTest {


    @BeforeClass
    public static void simpleTestBeforeClass()
    {
        System.out.println("simpleTestBeforeClass");
    }
    @Before
    public void simpleTestBefore()
    {
        System.out.println("simpleTestBefore");
    }
    @Test
    public void simpleTest2()
    {
        System.out.println("simpleTest2");
    }

    @Test
    public void simpleTest1()
    {
        System.out.println("simpleTest1");
    }

    @After
    public void simpleTestAfter()
    {
        System.out.println("simpleTestAfter");
    }
    @AfterClass
    public static void simpleTestAfterClass()
    {
        System.out.println("simpleTestAfterClass");
    }
}
