package com.tw.travelportal.test;

import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateConverterUnitTest {

    @Test
    public void convertLocalDateToDate()
    {
        LocalDate dateToConvert = LocalDate.now();
        Date convertedDate = java.util.Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        assert convertedDate instanceof java.util.Date;
    }

    @Test
    public void convertDateToLocalDate()
    {
        Date dateToConvert = new Date();
        LocalDate convertedDate = dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        assert convertedDate instanceof java.time.LocalDate;
    }
}
