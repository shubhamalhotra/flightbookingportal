package com.tw.travelportal.test;


import com.tw.travelportal.enums.TRAVEL_CLASS;
import com.tw.travelportal.model.FlightDetailWithCalculatedPrice;
import com.tw.travelportal.model.SearchCriteria;
import com.tw.travelportal.services.FlightSearchService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TotalPriceCalculatorIntgTest {

    @Autowired
    private FlightSearchService flightSearchService;

    @BeforeClass
    public static void init()
    {

    }

    @Test
    public void economyClassPriceCalculatorTest()
    {
        Map<String,Double> flightNamePriceMapper = new HashMap<>();
         flightNamePriceMapper.put("F001",8000.0);
         flightNamePriceMapper.put("F004",10000.0);
         flightNamePriceMapper.put("F007",15600.0);

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Hyderabad");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(2);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.ECONOMY_CLASS.name());

        List<FlightDetailWithCalculatedPrice> availableList = flightSearchService.getFlights(searchCriteria);

        for(Iterator<FlightDetailWithCalculatedPrice> calculatedPriceIterator = availableList.iterator(); calculatedPriceIterator.hasNext();)
        {
            FlightDetailWithCalculatedPrice flightDetailWithCalculatedPrice = calculatedPriceIterator.next();
            String flightName = flightDetailWithCalculatedPrice.getFlightDetail().getFlightName();
            double retrivedPrice = flightDetailWithCalculatedPrice.getFinalCalculatedPrice();
            double expectedPrice = flightNamePriceMapper.get(flightName);
            assert (retrivedPrice == expectedPrice);
        }
    }

    @Test
    public void businessClassPriceCalculatorTest()
    {
        Map<String,Double> flightNamePriceMapper = new HashMap<>();
        flightNamePriceMapper.put("F005",20000.0);
        flightNamePriceMapper.put("F008",26000.0);

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Hyderabad");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(2);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.BUSINESS_CLASS.name());

        List<FlightDetailWithCalculatedPrice> availableList = flightSearchService.getFlights(searchCriteria);

        for(Iterator<FlightDetailWithCalculatedPrice> calculatedPriceIterator = availableList.iterator(); calculatedPriceIterator.hasNext();)
        {
            FlightDetailWithCalculatedPrice flightDetailWithCalculatedPrice = calculatedPriceIterator.next();
            String flightName = flightDetailWithCalculatedPrice.getFlightDetail().getFlightName();
            double retrivedPrice = flightDetailWithCalculatedPrice.getFinalCalculatedPrice();
            double expectedPrice = flightNamePriceMapper.get(flightName);
            assert (retrivedPrice == expectedPrice);
        }
    }

    @Test
    public void firstClassPriceCalculatorTest()
    {
        Map<String,Double> flightNamePriceMapper = new HashMap<>();
        flightNamePriceMapper.put("F009",60000.0);

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setOrigin("Hyderabad");
        searchCriteria.setDestination("Mumbai");
        searchCriteria.setNumOfPassengers(2);
        searchCriteria.setDepartDate("2019-09-10");
        searchCriteria.setTravelClass(TRAVEL_CLASS.FIRST_CLASS.name());

        List<FlightDetailWithCalculatedPrice> availableList = flightSearchService.getFlights(searchCriteria);

        for(Iterator<FlightDetailWithCalculatedPrice> calculatedPriceIterator = availableList.iterator(); calculatedPriceIterator.hasNext();)
        {
            FlightDetailWithCalculatedPrice flightDetailWithCalculatedPrice = calculatedPriceIterator.next();
            String flightName = flightDetailWithCalculatedPrice.getFlightDetail().getFlightName();
            double retrivedPrice = flightDetailWithCalculatedPrice.getFinalCalculatedPrice();
            double expectedPrice = flightNamePriceMapper.get(flightName);
            assert (retrivedPrice == expectedPrice);
        }
    }
}
